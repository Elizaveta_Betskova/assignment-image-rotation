#include "../include/image.h"
#include "../include/status.h"
#include <stdint.h>
#include <stdio.h>

#define BF_TYPE 0x4D42
#define BI_BIT_COUNT 24
#define BI_PLANES 1
#define BMP_HEADER_SIZE 40

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)


uint8_t padding_count(uint64_t width){
    return width % 4;
}

struct bmp_header create_header(uint64_t width, uint64_t height){
    struct bmp_header new_header;
    const uint8_t padding = padding_count(width);
    const uint32_t size = width * sizeof(struct pixel)*height + padding*height;
    const uint32_t file_size = (sizeof(struct bmp_header) + size);

    new_header.bfType = BF_TYPE;
    new_header.bfileSize = file_size;
    new_header.bfReserved = 0;
    new_header.bOffBits = sizeof(struct bmp_header);
    new_header.biSize = BMP_HEADER_SIZE;
    new_header.biWidth = width;
    new_header.biHeight = height;
    new_header.biPlanes = BI_PLANES;
    new_header.biBitCount = BI_BIT_COUNT;
    new_header.biCompression = 0;
    new_header.biSizeImage = size;
    new_header.biXPelsPerMeter = 0;
    new_header.biYPelsPerMeter = 0;
    new_header.biClrUsed = 0;
    new_header.biClrImportant = 0;

    return new_header;
}

enum read_status valid_header(struct bmp_header* my_header){
    if (my_header->biBitCount != BI_BIT_COUNT || my_header->biPlanes != BI_PLANES || my_header->bfType != BF_TYPE){
        return READ_INVALID_HEADER;
    }
    return READ_OK;

}

enum read_status from_bmp(FILE* in , struct image* img){
    struct bmp_header in_header = {0};
    fread(&in_header, sizeof(struct bmp_header), 1, in);
    enum read_status header_status = valid_header(&in_header);
    if (header_status != READ_OK){
        return header_status;
    }

    *img = create_image(in_header.biWidth, in_header.biHeight);
    uint8_t padding = padding_count(img->width);
    for (uint64_t i = 0; i < in_header.biHeight; i ++){
        if (fread(&img->data[i*img->width], sizeof (struct pixel), in_header.biWidth, in) != in_header.biWidth){
            return READ_INVALID_BITS;
        }
        if (fseek(in, padding, SEEK_CUR) != 0){
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}


enum write_status to_bmp(FILE* out, struct image* img){
    struct bmp_header header = create_header(img->width, img->height);
    const uint32_t extra = 0;
    fwrite(&header, sizeof(struct bmp_header), 1, out);
    uint8_t padding = padding_count(img->width);
    for (uint64_t i = 0; i < img->height; i ++){
        if (!fwrite(&(img->data[i*img->width]), sizeof(struct pixel), img->width, out)) {
            return WRITE_ERROR;
        }
        if (fwrite(&extra, 1, padding, out) != padding){
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
