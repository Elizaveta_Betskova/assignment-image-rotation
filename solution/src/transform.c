#include "../include/transform.h"
#include "../include/image.h"
#include <stdint.h>


uint64_t get_old_pixel(uint64_t i, uint64_t j, uint32_t height){
    return i * height + j;
}

uint64_t get_new_pixel(uint64_t i, uint64_t j, uint32_t width){
    return j * width + width - i - 1;
}



struct image transform_image(struct image old_img){
    uint32_t width = old_img.height;
    uint32_t height = old_img.width;

    struct image new_img = create_image(width, height);

    for (uint64_t i = 0; i < old_img.height; i++) {
        for (uint64_t j = 0; j < old_img.width; j++) {
            new_img.data[get_new_pixel(i,j,width)] = old_img.data[get_old_pixel(i,j,height)];
        }
    }

    return new_img;
}
