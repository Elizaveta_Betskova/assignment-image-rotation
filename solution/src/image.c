#include "../include/image.h"
#include <malloc.h>
#include  <stdint.h>

struct image create_image (uint32_t w, uint32_t h){
    struct image img = make_zero_img();
    img.width = w;
    img.height = h;
    img.data =  malloc(sizeof(struct pixel)*w*h);
    return img;
}

struct image make_zero_img (){
    return (struct image) {.width = 0, .height = 0, .data = NULL};
}

void destroy_image(struct image* img){
    img->width = 0;
    img->height = 0;
    free((*img).data);
}
