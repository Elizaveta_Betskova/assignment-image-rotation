#include "../include/image.h"
#include "../include/status.h"
#include "../include/transform.h"
#include <stdio.h>

void usage() {
    fprintf(stderr, "Usage: ./image-transformer <source-image> <transformed-image>\n");
}

int main( int argc, char** argv ) {
    if (argc != 3){
        usage();
    }


    struct image image = make_zero_img();
    struct image trams_one = make_zero_img();

    FILE* in = fopen(argv[1], "rb");
    FILE* out = fopen(argv[2], "wb");

    if (from_bmp(in, &image) == READ_OK){
        trams_one = transform_image(image);
    }
    else {
        fclose(in);
        fclose(out);
        destroy_image(&image);
        return 1;
    }
    
    if (to_bmp(out, &trams_one) != WRITE_OK){
        fclose(in);
        fclose(out);
        destroy_image(&image);
        destroy_image(&trams_one);
        return 1;
    }

    fclose(in);
    fclose(out);

    destroy_image(&image);
    destroy_image(&trams_one);

    return 0;
}
