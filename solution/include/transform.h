#ifndef TEST_ROTATION_TRANSFORM_H
#define TEST_ROTATION_TRANSFORM_H
#include "../include/image.h"

struct image transform_image(struct image old_img);

#endif //TEST_ROTATION_TRANSFORM_H
