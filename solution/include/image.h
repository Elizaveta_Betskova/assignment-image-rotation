#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };
#pragma pack(pop)

struct image{
    uint64_t width, height;
    struct pixel* data;
};

struct image create_image(uint32_t w, uint32_t h);
struct image make_zero_img();
void destroy_image (struct image* img);


#endif //IMAGE_TRANSFORMER_IMAGE_H
